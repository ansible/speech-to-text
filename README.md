# Speech-to-text

Speech-to-text with `nerd-dictation` allows you to start dictating in your
choice of language (see VOSK's list of languages) using a keyboard
shortcut. The text output appears at the cursor in the focused window
(for example, your text editor).

This Ansible role installs `nerd-dictation` and its dependencies in a Python virtualenv,
installs language models of your choice and optionally enables keyboard shortcuts
for each language in your i3wm config file (this last part is very much tailored
to my own use, feel free to clone and adjust to yours).

I suggest you reset the `stt` dict in your `group_vars`/`host_vars` or similar,
for example (you only need to specify fields you want to override):
```
stt:
  nerd:
    repo:
      local: "{{ ansible_env.HOME }}/nerd-dictation"
    model:
      - name: vosk-model-small-en-us-0.15
        url: "https://alphacephei.com/vosk/models/vosk-model-small-en-us-0.15.zip"
        i3wm_shortcut: "$ms+Shift+e"
    desktop_shortcut:
      i3wm: false
```


## Links and notes

+ https://github.com/ideasman42/nerd-dictation
+ https://github.com/alphacep/vosk-api
+ https://alphacephei.com/vosk/models
+ https://github.com/julius-speech/julius
+ https://pypi.org/project/vosk
+ https://github.com/ccoreilly/vosk-browser
+ https://github.com/papoteur-mga/elograf
+ https://unix.stackexchange.com/questions/256138/is-there-any-decent-speech-recognition-software-for-linux
+ https://forums.raspberrypi.com/viewtopic.php?t=298045
+ https://anuran-roy.github.io/post/blog/offline-speech-with-vosk
+ https://news.ycombinator.com/item?id=29972579
+ https://fosspost.org/open-source-speech-recognition
+ https://unix.stackexchange.com/questions/256138/is-there-any-decent-speech-recognition-software-for-linux

For the opposite (i.e., text-to-speech, see for example https://github.com/rhasspy/larynx)
